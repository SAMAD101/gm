# create_wallet.py
#
# Copyright 2023 Sam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk, Adw
from gettext import gettext as _
from solathon import Client, Keypair
import json, os


@Gtk.Template(resource_path='/org/shastraos/gm/pages/create_screen.ui')
class CreateWalletScreen(Adw.Bin):
    __gtype_name__ = 'CreateWalletScreen'
    acc_name = Gtk.Template.Child()
    password = Gtk.Template.Child()
    next_button = Gtk.Template.Child()
    back_button = Gtk.Template.Child()
    loader = Gtk.Template.Child()
    
    username = "Account 1"
    passwd = ""
    
    def __init__(self, window, main_carousel, application, **kwargs):
        super().__init__(**kwargs)
        self.carousel = main_carousel
        
        self.acc_name.connect('changed', self.get_acc_name)
        self.password.connect('changed', self.get_password)
        self.next_button.connect('clicked', self.next_button_clicked)
        self.back_button.connect('clicked', self.back_button_clicked)
                
    def get_acc_name(self, *args):
        self.username = args[0].get_text()
        self.verify(self.username, self.passwd)
        
    def get_password(self, *args):
        self.passwd = args[0].get_text()
        self.verify(self.username, self.passwd)
        
    def next_button_clicked(self, *args):
        self.next_button.set_sensitive(False)
        self.loader.set_visible(True)
        self.loader.start()
        
        # save the account details
        client = Client("https://api.devnet.solana.com")
        
        new_account = Keypair()
        # print(new_account.public_key, new_account.private_key)
        amount = 10000  # This is the amount in lamports
    
        res = client.request_airdrop(new_account.public_key, amount)
        print("Airdrop response: ", res)
        
        json_values = {
            "account_name": self.username,
            "public_key": str(new_account.public_key.base58_encode()),
            "private_key": str(new_account.private_key.base58_encode()),
            "password": self.passwd
        }
        
        file_path = os.path.join("/","home", os.getlogin(), "acc.json")
        
        json.dump(json_values, open(file_path, "w"), indent=4)
            
        self.loader.stop()

        self.carousel.scroll_to(self.carousel.get_nth_page(5), True)
    
    def back_button_clicked(self, *args):
        self.carousel.scroll_to(self.carousel.get_nth_page(0), True)

    def verify(self, username, password):
        if username != "" and len(password)>=8:
            self.next_button.set_sensitive(True)
        else:
            self.next_button.set_sensitive(False)