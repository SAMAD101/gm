# import_wallet.py
#
# Copyright 2023 Sam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk, Adw
import re


@Gtk.Template(resource_path='/org/shastraos/gm/pages/import_screen.ui')
class ImportWalletScreen(Adw.Bin):
    __gtype_name__ = 'ImportWalletScreen'
    recovery_phrase12 = Gtk.Template.Child()
    enable_24word_recovery_phrase = Gtk.Template.Child()
    recovery_phrase24 = Gtk.Template.Child()
    next_button = Gtk.Template.Child()
    back_button = Gtk.Template.Child()

    
    def __init__(self, window, main_carousel, application, **kwargs):
        super().__init__(**kwargs)
        self.carousel = main_carousel
        
        self.recovery_phrase12.connect("changed", self.get_recovery_phrase)
        self.enable_24word_recovery_phrase.connect("notify::active", self.change_recovery_phrase_variant)
        self.recovery_phrase24.connect("changed", self.get_recovery_phrase)
        self.next_button.connect("clicked", self.next_button_clicked)
        self.back_button.connect("clicked", self.back_button_clicked)
        
    def get_recovery_phrase(self, *args):
        if self.enable_24word_recovery_phrase.get_active():
            __recovery_phrase = self.recovery_phrase24.get_text()
        else:
            __recovery_phrase = self.recovery_phrase12.get_text()
        self.verify(__recovery_phrase)
            
    def change_recovery_phrase_variant(self, switch, *args):
        if switch.get_active():
            self.recovery_phrase24.set_sensitive(True)
            self.recovery_phrase12.set_sensitive(False)
        else:
            self.recovery_phrase24.set_sensitive(False)
            self.recovery_phrase12.set_sensitive(True)

    def next_button_clicked(self, *args):
        self.carousel.scroll_to(self.carousel.get_nth_page(3), True)
        
    def back_button_clicked(self, *args):
        self.carousel.scroll_to(self.carousel.get_nth_page(0), True)
        
    def verify(self, *args):
        if self.enable_24word_recovery_phrase.get_active():
            if re.serach(r'^(\S+\s+){23}\S+$', args[0]):
                # pass the recovery phrase for importing of account
                self.next_button.set_sensitive(True)
                print("Valid recovery phrase")
            else:
                self.next_button.set_sensitive(False)
        elif not self.enable_24word_recovery_phrase.get_active():
            if re.search(r'^(\S+\s+){11}\S+$', args[0]):
                # pass the recovery phrase for importing of account
                self.next_button.set_sensitive(True)
                print("Valid recovery phrase")
            else:
                self.next_button.set_sensitive(False)
                