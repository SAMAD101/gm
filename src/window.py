# window.py
#
# Copyright 2023 Sam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk, Adw
from .functions.import_wallet_screen import ImportWalletScreen
from .functions.create_wallet_screen import CreateWalletScreen
from .functions.account_screen import AccountScreen
from .functions.summary_screen import SummaryScreen
from .functions.finish_screen import FinishScreen


@Gtk.Template(resource_path='/org/shastraos/gm/window.ui')
class ShastraosGmWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'ShastraosGmWindow'

    carousel = Gtk.Template.Child()
    welcome_page = Gtk.Template.Child()
    # import_wallet_button = Gtk.Template.Child()
    create_wallet_button = Gtk.Template.Child()
        
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.finished_screen = FinishScreen(window=self, main_carousel=self.carousel, **kwargs)
        self.summary_screen = SummaryScreen(window=self, main_carousel=self.carousel, **kwargs)
        self.account_screen = AccountScreen(window=self, main_carousel=self.carousel, **kwargs)
        self.import_wallet_screen = ImportWalletScreen(window=self, main_carousel=self.carousel, **kwargs)
        self.create_wallet_screen = CreateWalletScreen(window=self, main_carousel=self.carousel, **kwargs)
        self.carousel.append(self.import_wallet_screen)
        self.carousel.append(self.create_wallet_screen)
        self.carousel.append(self.account_screen)
        self.carousel.append(self.summary_screen)
        self.carousel.append(self.finished_screen)
        
        """
        Page indices:
        Starting (welcome) page: 0
        Import wallet page: 1
        Create wallet page: 2
        Account page: 3
        Summary page: 4
        Finish page: 5
        """
        
        # self.import_wallet_button.connect("clicked", self.launch_wallet_import)
        self.create_wallet_button.connect("clicked", self.launch_wallet_creation)
        
    def launch_wallet_import(self, *args):
        self.carousel.scroll_to(self.carousel.get_nth_page(1), True)
    
    def launch_wallet_creation(self, *args):
        self.carousel.scroll_to(self.carousel.get_nth_page(2), True)
 